import json
import requests
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    # use request lib to get photo
    header = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": (city + " " + state),
        "per_page": 1
    }
    response = requests.get("https://api.pexels.com/v1/search", params=params, headers=header)
    content = response.json()
    try:
        return { "picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

def get_weather(city, state):
    lat_lon_url = f"https://api.openweathermap.org/geo/1.0/direct?q={city},US-{state},USA&appid={OPEN_WEATHER_API_KEY}"
    lat_lon_response = requests.get(lat_lon_url)

    loc_content = lat_lon_response.json()

    if not loc_content:
        return None

    try:
        lat = loc_content[0]["lat"]
        lon = loc_content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url, params={"units": "imperial"})
    content = response.json()

    if not content:
        return None

    try:
        weather_description = content["weather"][0]["description"]
        temperature = content["main"]["temp"]
        return {
            "description": weather_description,
            "temp": temperature,
        }
    except (KeyError, IndexError):
        return None



# def get_weather_data(city, state):
#     # Base URLs for OpenWeatherMap APIs
#     geocoding_api_url = "http://api.openweathermap.org/geo/1.0/direct"
#     geocoding_params = {
#         "q": f"{city}, {state.abbreviation}",
#         "limit": 1,  # Limit to one result
#         "appid": OPEN_WEATHER_API_KEY
#     }

#     geocoding_response = requests.get(geocoding_api_url, params=geocoding_params)
#     geocoding_response.raise_for_status()

#     geocoding_data = geocoding_response.json()

#     if not geocoding_data:  # Check for empty results
#         return {"error": "Location not found"}  # Or raise an exception

#     lat = geocoding_data[0]["lat"]
#     lon = geocoding_data[0]["lon"]

#     # Step 2: Fetch weather data using coordinates
#     weather_api_url = "https://api.openweathermap.org/data/2.5/weather"
#     weather_params = {
#         "lat": lat,
#         "lon": lon,
#         "appid": OPEN_WEATHER_API_KEY,
#         "units": "imperial"  # Or "metric"
#     }

#     weather_response = requests.get(weather_api_url, params=weather_params)
#     weather_response.raise_for_status()

#     weather_data = weather_response.json()

#     # Step 3: Extract weather information
#     return {
#         "temperature": weather_data["main"]["temp"],
#         "description": weather_data["weather"][0]["description"]
#     }
# def get_weather_data(city, state):

#     # Create the URL for the geocoding API with the city and state
#     # Make the request
#     # Parse the JSON response
#     # Get the latitude and longitude from the response

#     # Create the URL for the current weather API with the latitude
#     #   and longitude
#     # Make the request
#     # Parse the JSON response
#     # Get the main temperature and the weather's description and put
#     #   them in a dictionary
#     # Return the dictionary
